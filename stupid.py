#!/usr/bin/python3


def translate(texts, dictionary, address):
    lines = texts.read().splitlines()
    print(lines)
    out = open(address + '/out.tsv', 'w', encoding='utf-8')
    for line in lines:
        print(line + '\n')
        line = line.lower()
        for source, target in dictionary:
            line = line.replace(source, target)
        print(line + '\n')
        out.write(line + '\n')
    out.close()


def prepareDictionary():
    dictionaryFile = open('dict.csv', 'r', encoding='utf-8')
    dictionary = []
    for word in dictionaryFile.readlines():
        word = word.replace("\n", "")
        wordSplitted = word.split(',')
        source = wordSplitted[0]
        target = wordSplitted[1]
        pair = [source, target]
        dictionary.append(pair)
    print(dictionary)
    return dictionary


def main():
    dictionary = prepareDictionary()

    dev0 = open('dev-0/in.tsv', 'r', encoding='utf-8')
    translate(dev0, dictionary, 'dev-0')

    testA = open('test-A/in.tsv', 'r', encoding='utf-8')
    translate(testA, dictionary, 'test-A')


main()
